Official website: ( https://freecommander.com ).

-Download latest setup (FreeCommanderXE-32-public_setup.zip) from: ( https://freecommander.com/en/downloads ).
-Unzip downloaded archive and run setup.
-Press "Yes".
-Select "English" language and press "OK".
-Select "I accept the agreement". Press "Next".
-Set installation directory to ( C:\Tools\FreeCommanderXE ). Press "Next". It prompts that the folder does not exists, press "Yes".
-Press "Next".
-If needed check "Create a desktop icon", although it is recommended to create a shortcut directly in the taskbar. Press "Next".
-Press "Install".
-Press "Finish".
-Copy all the contents from "<Library_Tools_Path>\FreeCommanderXE\Total7zip" folder to "C:\Tools\FreeCommanderXE" (after this step you need to have "C:\Tools\FreeCommanderXE\Plugins" and "C:\Tools\FreeCommanderXE\total7zip.xml").
-Open FreeCommander. Go to "Tools" -> "Restore all settings...". Select the "<Library_Tools_Path>\FreeCommanderXE\Settings" folder. Press "OK". If prompts to overwrite existing files press "Yes".

---------------------------------------------------------------------------------------------------

-A full list of configured shortcuts can be seen and tweaked from "Tools" -> "Define keyboard shortcuts..." (they're part of the imported settings).
-The following programs, which are used by Free Commander XE, shall also be installed according to the given "Setup.txt" files:
   -The default editor and viewer is "Notepad++".
   -The default compare software is "Beyond Compare 4".
   -The default packing / unpacking software is "7-Zip".
-More tweaks and configurations are possible in "Tools" -> "Settings".