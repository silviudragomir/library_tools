Official website: ( https://www.python.org ).

-Go to ( https://www.python.org/downloads/windows ). Select latest release. Select "Windows x86-64 executable installer".
-Run downloaded setup.
-Select "Customize installation".
-Check all checkboxes. Press "Next".
-Check all checkboxes. Set installation directory to ( C:\Tools\Python ). Press "Install".
-Press "Close".
-Open Windows cmd. Type "pip install xlwings". Enter.
-Close Windows cmd.